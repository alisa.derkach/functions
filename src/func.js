const getSum = (str1, str2) => {
  if (typeof str1 !== 'string' || typeof str2 !== 'string') {
    return false;
} 
if (str1.length == 0 || str2.length == 0) {
    str1.length === 0 ? str1 = 0 : str2 = 0;
}
if (Number(str1) && Number(str2) || (str1 === 0 || str2 === 0) ) {
    let result = Number(str1) + Number(str2);
    result = result.toString();
    return `${result}`;
} else {
    return false;
}
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let post = 0; 
    let arr = [];
    for (let el of listOfPosts) {
        if (el.author == authorName) {
            post ++;   
        }
    }
  let comments = listOfPosts.reduce((quantityOfComments, post) => {
    console.log(post)
    return quantityOfComments + (post.comments === undefined ? 0 : post.comments.filter(comment => comment.author === authorName).length);
  }, 0);
    return `Post:${post},comments:${comments}`;
};

const tickets=(people)=> {
  let sum = 0;
  for (el of people ) {
      if (+el > 25 ) {
          if (+el - 25 <= sum) {
          } else {
              return 'NO'
          }
      }
      sum += 25;
  }; 
  return 'YES'
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
